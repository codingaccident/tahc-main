package com.tahc.main.network.packets;

import amp.headless_amplet.HeadlessAmpletTwinReadable;
import amp.headless_amplet.HeadlessAmpletWritable;
import com.tahc.main.data.ChatEntry;
import com.tahc.main.data.InvalidAmpBuildException;

public class ChatPacket implements Packet {
    private ChatEntry chatEntry;
    @Override
    public void build(byte[] serialized) throws InvalidAmpBuildException {
        HeadlessAmpletTwinReadable har = HeadlessAmpletTwinReadable.create(serialized);
        long timestamp = har.getNextLong();
        String IDfrom = har.getNextString(har.getNextInt());
        String IDto = har.getNextString(har.getNextInt());
        String text = har.getNextString(har.getNextInt());
        chatEntry = new ChatEntry(IDfrom,IDto,text,timestamp);
    }

    @Override
    public byte[] serializeToBytes() {
        HeadlessAmpletWritable haw = HeadlessAmpletWritable.create();
        haw.addElement(chatEntry.getTimestamp().getValue());
        haw.addElement(chatEntry.getIDfrom().length());
        haw.addElement(chatEntry.getIDfrom());
        haw.addElement(chatEntry.getIDto().length());
        haw.addElement(chatEntry.getIDto());
        haw.addElement(chatEntry.getText().getValue().length());
        haw.addElement(chatEntry.getText().getValue());
        return haw.serializeToBytes();
    }
}
