package com.tahc.main.network.logic;

import com.tahc.main.data.AmpBuildable;

public class ConnMan implements IConnectionManager {

    private INetworkEndpoint endpoint;
    public ConnMan(INetworkEndpoint endpoint)
    {
        this.endpoint = endpoint;
    }

    @Override
    public void connected() {

    }

    @Override
    public void received(Object packet) {

    }

    @Override
    public void send(AmpBuildable packet) {
        endpoint.sendPacket(packet);
    }


}
