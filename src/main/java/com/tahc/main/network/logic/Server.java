package com.tahc.main.network.logic;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.udt.nio.NioUdtProvider;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;

public class Server {

    private static final boolean SSL = System.getProperty("ssl") != null;
    private int port;

    public Server(int port)
    {
        this.port = port;
    }

    private ChannelFuture future;
    public void start() throws Exception {
        // Configure SSL.
        final SslContext sslCtx;
        if (SSL) {
            SelfSignedCertificate ssc = new SelfSignedCertificate();
            sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build();
        } else {
            sslCtx = null;
        }

        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();


//            b.group(workerGroup)
////                    .channel(NioServerSocketChannel.class)
////                    .channelFactory(NioUdtProvider.MESSAGE_ACCEPTOR)
//                    .channel(NioDatagramChannel.class)
//                    .handler(new LoggingHandler(LogLevel.INFO))
//                    .childHandler(new ChannelInitializer<SocketChannel>() {
//                        @Override
//                        public void initChannel(SocketChannel ch) throws Exception {
//                            ChannelInboundHandlerAdapter serverHandler;
//
//                                serverHandler = new ServerHandler();
//                            ChannelPipeline p = ch.pipeline();
//
//                            p.addLast(
//                                    new LengthFieldPrepender(4),
//                                    new PacketDecoder(150_000_000, 0, 4, 0, 0),
//                                    serverHandler);
//                            if (sslCtx != null) {
//                                p.addLast(sslCtx.newHandler(ch.alloc()));
//                            }
//
//                        }
//                    }).childOption(ChannelOption.SO_KEEPALIVE,true);

            // Bind and start to accept incoming connections.
            future = b.bind(port).sync();
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public void close() {
        if(future == null) return;
        future.channel().close();
    }


}
