package com.tahc.main.network.logic;

import com.tahc.main.data.AmpBuildable;

public interface IConnectionManager {

    void connected();
    void received(Object packet);
    void send(AmpBuildable packet);
}
