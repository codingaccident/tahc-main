package com.tahc.main;

import com.bitbucket.backspace119.mqttchat.MQTTBaseClient;
import com.bitbucket.backspace119.mqttchat.MQTTChat;
import com.bitbucket.backspace119.mqttchat.MessageConsumer;

public interface ITAHC {

    MQTTChat getChat();
    MQTTBaseClient getChatClient();
    void setConsumer(MessageConsumer consumer);
}
