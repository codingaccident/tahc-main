package com.tahc.main;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;

import java.nio.charset.StandardCharsets;

public class MainController {
    public JFXButton connectButton;
    public JFXTextArea chatArea;
    public JFXTextField chatInput;
    public JFXTextField usernameText;
    public JFXTextArea connectedUsers;
    private ITAHC tahc;

    public void init(ITAHC tahc)
    {
        this.tahc = tahc;
        tahc.setConsumer((typ,dat,top) -> chatArea.appendText(new String(dat, StandardCharsets.UTF_8) + "\n"));
    }

    public void chatSubmitted()
    {
        tahc.getChat().sendChat("general",chatInput.getText());
        chatInput.clear();
    }

    public void changeUsername(ActionEvent actionEvent) {
        if(!usernameText.getText().isEmpty())
        tahc.getChatClient().getUser().setUserName(usernameText.getText());
    }
}
