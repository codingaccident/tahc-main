package com.tahc.main;

import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.IOException;

public class App extends Application {


    private static ITAHC tahc;
    public static void main(String[] args) {
        parseArgs(args);

        System.out.println("Welcome to EXTREME I/O chat. GET READY TO HAVE YOUR EARDRUMS BLEED. Type exit to quit");
        try {
            tahc = new TAHC("tcp://ampextech.ddns.net:1883","Anonymous","12345".toCharArray());
        } catch (MqttException e) {
            e.printStackTrace();
        }
        launch(args);


    }

    private static void parseArgs(String[] args)
    {
        for(String arg:args)
        {

        }
    }


    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("/MainPage.fxml"));
        AnchorPane main = loader.load();
        MainController controller = loader.getController();
        JFXDecorator decorator = new JFXDecorator(primaryStage, main);

        decorator.setOnCloseButtonAction(() -> {
            tahc.getChatClient().disconnect();
            Platform.exit();
        });
        controller.init(tahc);
        Scene pScene = new Scene(decorator,1600,900);
        primaryStage.setScene(pScene);
        primaryStage.show();

    }
}
