package com.tahc.main.data;

import com.tahc.main.data.encryption.EncryptionManager;
import com.tahc.main.data.events.Watchable;
import com.tahc.main.data.events.Watcher;

import java.util.ArrayList;
import java.util.List;

/**
 * Watchers of this class will receive chat entries as we receive them, not an updated list
 */
public class ChatParticipant implements Watcher<ChatEntry>, Watchable<ChatEntry> {
    private List<Watcher<ChatEntry>> watchers = new ArrayList<>();
    private List<Watcher<ChatEntry>> singleWatchers = new ArrayList<>();
    private String ID;
    private String IDto;

    public ChatParticipant(String ID, String IDto)
    {
        this.ID = ID;
        this.IDto = IDto;

    }
    public ChatParticipant(String IDto)
    {
        ID = EncryptionManager.sha256(System.nanoTime() + "insert_salt_here");
        this.IDto = IDto;
    }

    public String getID() {
        return ID;
    }

    public String getIDto() {
        return IDto;
    }

    @Override
    public void addWatcher(Watcher<ChatEntry> w) {
        watchers.add(w);
    }

    @Override
    public void addSingleEventWatcher(Watcher<ChatEntry> w) {
        singleWatchers.add(w);
    }

    @Override
    public void update(Watchable<ChatEntry> o, ChatEntry chat) {
        for(Watcher<ChatEntry> w:watchers)
        {
            w.update(o,chat);
        }
    }

    public void addChat(String chat,String IDfrom, String IDto)
    {
        ChatEntry ce = new ChatEntry(IDfrom,IDto,chat);
        update(this,ce);

    }
}
