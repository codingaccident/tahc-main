package com.tahc.main.data.encryption;

import com.tahc.main.data.Utils;
import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.zip.CRC32;

/**
 * Created by Bryan on 5/30/2017.
 */
public class EncryptionManager{

    public static final String KEY_FILE = "keys/key";
    public static final String KEY_PADDING = "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE";
    public static final String KEY_PROTOCOL = "brainpoolp512t1";
    public static final String ED_PROTOCOL = "Ed25519";

    public EncryptionManager(){
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public static void initStatic() {
        Security.addProvider(new BouncyCastleProvider());
    }

    static {
        Security.addProvider(new BouncyCastleProvider());

    }


    public static int sha256NoNew(String input,byte[] buf)
    {
        if(md == null) return 0;
        try {
            md.update(input.getBytes("UTF-8"));

            //logger.debug("Size of hash is: " + digest.length);
            int r = md.digest(buf,0,buf.length);
            md.reset();
            return r;
        } catch (UnsupportedEncodingException e) {


        } catch (DigestException e) {
            e.printStackTrace();
        }
        return 0;
    }

    static SHA3.DigestSHA3 md = new SHA3.Digest512();

    public static byte[] sha512NoNew(byte[] input) {

        md.update(input);
        byte[] hash = md.digest();
        md.reset();
        return hash;
    }

    public static String sha512(String input)
    {
        SHA3.DigestSHA3 md = null;
        try {
            md = new SHA3.Digest512();
            md.update(input.getBytes("UTF-8"));
            byte[] digest = md.digest();
            //logger.debug("Size of hash is: " + digest.length);
            return Utils.toBase64(digest);
        } catch (UnsupportedEncodingException e) {


        }
        return null;
    }

    public static byte[] sha512(byte[] input)
    {
        SHA3.DigestSHA3 md = new SHA3.Digest512();
        md.update(input);
        return md.digest();
    }

    public static String sha224(String input)
    {
        SHA3.DigestSHA3 md = null;
        try {
            md = new SHA3.Digest224();
            md.update(input.getBytes("UTF-8"));
            byte[] digest = md.digest();
            //logger.debug("Size of hash is: " + digest.length);
            return Utils.toBase64(digest);
        } catch (UnsupportedEncodingException e) {


        }
        return null;
    }

    public static String sha3256(String input) {
        SHA3.DigestSHA3 md = null;
        try {
            md = new SHA3.Digest256();
            md.update(input.getBytes("UTF-8"));
            byte[] digest = md.digest();
            //logger.debug("Size of hash is: " + digest.length);
            return Utils.toBase64(digest);
        } catch (UnsupportedEncodingException e) {


        }
        return null;
    }

    public static byte[] sha3256(byte[] input) {
        //System.out.println("Call to sha3256");
        SHA3.DigestSHA3 md = new SHA3.Digest256();
        //System.out.println("digest created");
        md.update(input);
        //System.out.println("digest updated with input");
        return md.digest();
    }

    public static String sha224Hex(String input)
    {
        SHA3.DigestSHA3 md = null;
        try {
            md = new SHA3.Digest224();
            md.update(input.getBytes("UTF-8"));
            byte[] digest = md.digest();
            //logger.debug("Size of hash is: " + digest.length);
            return Utils.toHexArray(digest);
        } catch (UnsupportedEncodingException e) {


        }
        return null;
    }

    public static String sha256(String input)
    {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(input.getBytes("UTF-8"));
            byte[] digest = md.digest();
            //logger.debug("Size of hash is: " + digest.length);
            return Utils.toBase64(digest);
        } catch (UnsupportedEncodingException e) {


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] sha224(byte[] input) {
        SHA3.DigestSHA3 md = new SHA3.Digest224();
        md.update(input);
        return md.digest();
    }

    public static byte[] sha384(byte[] input) {
        SHA3.DigestSHA3 md = new SHA3.Digest384();
        md.update(input);
        return md.digest();
    }



    public static long getCRCValue(byte[] data) {
        CRC32 crc = new CRC32();
        crc.update(data);
        return crc.getValue();
    }

    public static boolean checkCRCValue(byte[] data, long value) {
        CRC32 crc = new CRC32();
        crc.update(data);
        return crc.getValue() == value;
    }
}
