package com.tahc.main.data;

import com.tahc.main.data.events.Dispatcher;
import com.tahc.main.data.events.Watchable;
import com.tahc.main.data.events.Watcher;

import java.util.ArrayList;
import java.util.List;

public class OData<T> implements Watchable<T> {
    private List<Watcher<T>> watchers = new ArrayList<>();
    private List<Watcher<T>> singleWatchers = new ArrayList<>();
    private T v;
    //default implementation, all on same thread, most implementations will probably use multi threaded version
    private Dispatcher<T> dispatcher = (w,d) -> {
        for(Watcher<T> watcher:watchers) watcher.update(w,d);
        for(Watcher<T> watcher:singleWatchers) watcher.update(w,d);
        singleWatchers.clear();
    };

    public OData(T data)
    {
        v = data;
    }
    public OData(T data,Dispatcher<T> dispatcher)
    {
        v = data;
        this.dispatcher = dispatcher;
    }
    public OData(Dispatcher<T> dispatcher)
    {
        this.dispatcher = dispatcher;
    }
    @Override
    public synchronized void addWatcher(Watcher<T> w)
    {
        watchers.add(w);
    }

    @Override
    public synchronized void addSingleEventWatcher(Watcher<T> w)
    {
        singleWatchers.add(w);
    }

    public synchronized void setValue(T v)
    {
        this.v = v;
        dispatcher.dispatch(this,v);
    }

    public synchronized T getValue()
    {
        return v;
    }

}
