package com.tahc.main.data;

public class InvalidAmpBuildException extends Exception {
    public InvalidAmpBuildException(String msg) {
        super(msg);
    }
}
