package com.tahc.main.data;

import java.util.ArrayList;
import java.util.List;

public class Chat {
    private OData<List<OData<ChatEntry>>> chat = new OData<>(new ArrayList<>());
    private OData<List<ChatParticipant>> participants = new OData<>(new ArrayList<>());


    public void addParticipant(ChatParticipant participant)
    {
       List<ChatParticipant> val = participants.getValue();
       val.add(participant);
       participants.setValue(val);
    }

    public void removeParticipant(ChatParticipant participant)
    {
        List<ChatParticipant> val = participants.getValue();
        val.remove(participant);
        participants.setValue(val);
    }

    public OData<List<OData<ChatEntry>>> getChat() {
        return chat;
    }

    public OData<List<ChatParticipant>> getParticipants() {
        return participants;
    }
}
