package com.tahc.main.data;

import amp.serialization.IAmpByteSerializable;

public interface AmpBuildable extends IAmpByteSerializable {
    void build(byte[] serialized) throws InvalidAmpBuildException;
}
