package com.tahc.main.data;

import com.tahc.main.data.encryption.EncryptionManager;

public class ChatEntry {
    private OData<String> text;
    private OData<Long> timestamp;
    private String IDfrom;
    private String IDto;
    private String messageID;

    public ChatEntry(String IDfrom, String IDto, String text)
    {
        this.text = new OData<>(text);
        timestamp = new OData<>(System.currentTimeMillis());
        this.IDfrom = IDfrom;
        this.IDto = IDto;
        messageID = EncryptionManager.sha256(IDfrom + IDto + timestamp);
    }
    public ChatEntry(String IDfrom, String IDto, String text, long timestamp)
    {
        this.text = new OData<>(text);
        this.timestamp = new OData<>(timestamp);
        this.IDfrom = IDfrom;
        this.IDto = IDto;
        messageID = EncryptionManager.sha256(IDfrom + IDto + timestamp);
    }

    public String getMessageID() {
        return messageID;
    }

    public void setText(String text) {
        this.text.setValue(text);
    }

    public OData<String> getText() {
        return text;
    }

    public OData<Long> getTimestamp() {
        return timestamp;
    }

    public String getIDfrom() {
        return IDfrom;
    }

    public String getIDto() {
        return IDto;
    }
}
