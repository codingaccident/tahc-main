package com.tahc.main.data;

public class InvalidCRCException extends Exception {
    public InvalidCRCException(String msg) {
        super(msg);
    }
}
