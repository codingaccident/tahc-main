package com.tahc.main.data.events;

public interface Dispatcher<T> {
    void dispatch(Watchable<T> watchable, T data);
}
