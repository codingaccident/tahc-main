package com.tahc.main.data.events;

import com.tahc.main.data.OData;

@FunctionalInterface
public interface Watcher<T>  {
    void update(Watchable<T> o, T arg);
}
