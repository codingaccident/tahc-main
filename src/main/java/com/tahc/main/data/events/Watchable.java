package com.tahc.main.data.events;

public interface Watchable<T> {

    void addWatcher(Watcher<T> w);
    void addSingleEventWatcher(Watcher<T> w);
}
