package com.tahc.main;

import com.bitbucket.backspace119.mqttchat.*;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.*;

public class TAHC implements ITAHC {

    private MQTTChat chat;
    private MQTTBaseClient client;
    private User user;
    private Log log;
    private MessageConsumer consumer;

    public TAHC(String serverAddress,String username, char[] pass) throws MqttException {
        Set<String> subscriptions = new HashSet<>();
        subscriptions.add("general");
        log = new Log(){};
        user = new SimpleUser(username, UUID.randomUUID(),subscriptions);
        client = new MQTTBaseClient(user,pass,serverAddress,log);
        chat = new MQTTChat(client,user,(typ,dat,top) -> consumer.consume(typ,dat,top));
    }

    @Override
    public void setConsumer(MessageConsumer consumer)
    {
        this.consumer = consumer;
    }

    @Override
    public MQTTChat getChat() {
        return chat;
    }

    @Override
    public MQTTBaseClient getChatClient() {
        return client;
    }

}
